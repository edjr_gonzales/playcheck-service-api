export interface ServiceConfig {
  require_path:string;
  startup: boolean;
  base_path: string;
  args?:any[];
}