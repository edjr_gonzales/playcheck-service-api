import express, { Request, Response } from "express";
import session from "express-session";

export * from "./loadable-service";
export * from "./service-config";

export type ConnectMiddleware = (req:Request, resp:Response, next:Function) => any;

export interface MiddlewareMap {
  [id:string]: any;
}

export interface MiddlewareChainMap {
  public: ConnectMiddleware[],
  authenticated: ConnectMiddleware[],
  adminOnly: ConnectMiddleware[]
}

// express related
/* declare global {
  namespace Express {
      interface Request {
          session: session.Session & Partial<session.SessionData> & {
            userAuthId?:string;
          };
      }
  }
} */