import { Express } from "express";
import { MiddlewareMap, MiddlewareChainMap } from ".";

export interface LoadableService {
  serviceName:() => string;
  version:() => number;
  hotPlugControlFile:() => string;
  register:(express:Express, mountPath:string, middlewareChainMap:MiddlewareChainMap, middlewares?:MiddlewareMap) => void;
}
