import 'core-js';

import express from 'express';
import https from 'https';
import cors from 'cors';
import bodyParser from 'body-parser';
import session  from 'express-session';
import passport from 'passport';
import { createClient } from 'redis';
import connectRedis from 'connect-redis';

import fs from 'fs';
import path from 'path';

import { getBunyanLogger, isDevelopment, isTest, isProduction  } from "@playcheck/standard-lib";
import { LoadableService, MiddlewareChainMap, MiddlewareMap, ServiceConfig } from './types';

// load ENV config before everything else
require('dotenv').config();

const env = process.env.NODE_ENV;

const logger = getBunyanLogger('main-service-api', { env });
const server = express();

const port = process.env.API_HTTP_PORT || 8080;
const isHttps = !!process.env.API_HTTPS_PORT && !!process.env.API_HTTPS_CERT && !!process.env.API_HTTPS_KEY;
const hasRedis = !!process.env.REDIS_HOST;

const middlewares:MiddlewareMap = {
  passport
}

const middlewareChains:MiddlewareChainMap = {
  public: [],
  authenticated: [],
  adminOnly: []
}

if(hasRedis){
  logger.info(`Redis ENV are present. Will start a redis client to Host: ${process.env.REDIS_HOST} Port: ${process.env.REDIS_PORT || 6379}`);
}
const redisClient = hasRedis ? createClient({ url: process.env.REDIS_HOST }) : undefined;

const loadServices = () => {
  //load services.json
  logger.info("Loading service configuration...");
  fs.readFile('./services.json', (err, data) => {
    if (err) throw err;

    const services = JSON.parse(data.toString());
    logger.info("Loaded service configuration, processing...");

    Object.keys(services).forEach(key => {
      logger.info("service item: ", key);

      try {
        const { require_path, base_path, startup, args }:ServiceConfig = services[key];

        logger.debug("service config: ", require_path, base_path, startup, args);

        const { default: Service } = require(path.resolve(__dirname, require_path));
        logger.debug("Service", Service);

        const serviceInstance:LoadableService = !!args ? new Service(...args) : new Service();
        serviceInstance.register(server, base_path, middlewareChains, middlewares);
        logger.info("service registered", serviceInstance.serviceName(), serviceInstance.version());

      } catch(err){
        logger.error("service startup failed", err);
      }
    });
  });
}

/* build service middlewares **/

//cors
const corsWhiteList:string[] = [];
const loadCORS = () => {
  //logger.debug("Loading CORS configuration...");
  fs.readFile('./cors.json', (err, data) => {
    if (!err){
      //logger.info("Loaded CORS configuration, processing...");
      try {
        const corsList:string[] = JSON.parse(data.toString());
        corsWhiteList.splice(0, corsWhiteList.length);
        corsWhiteList.push(...corsList);
      } catch(e) {
        logger.info("CORS configuration error", e);
      }
    } else {
      logger.debug("Failed to load CORS configuration", err);
    }
  });
}

// start inittializing express and its middlewares

server.use(cors({ origin: (origin, callback:Function) => {
  if(corsWhiteList.length === 0){
    //depending whether its is dev or prod we will control this behavior
    //if it is dev/test, we will allow all
    if(isDevelopment(env) || isTest(env)){
      callback(null, true);
      return;
    }
  }

  // otherwise, filter it by cors and block it by default
  if (origin === undefined || corsWhiteList.indexOf(origin) !== -1) {
    callback(null, true)
  } else {
    callback(new Error('Blocked by CORS'))
  }

}}));

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }))

server.use(session({ 
  secret: process.env.SESSION_SECRET || "appsecret", 
  resave: false, 
  saveUninitialized:false, 
  rolling: true, 
  cookie: { maxAge: Number(process.env.DAY_EXPIRATION || 7), secure: isHttps },
  store: isProduction(env) ? ( hasRedis ? new (connectRedis(session))({ client: redisClient, prefix: 'playcheck-sessions' }) : undefined ) : undefined
}));
server.use(passport.initialize());
server.use(passport.session());

/* server.use((req, res, next) => {
  console.log("Incoming", req);
  next();
}) */

//load cors config
logger.debug("Loading CORS configuration...");
loadCORS();

// attach endpoint services
loadServices();

// listen
const app = server.listen(
  port, 
  () => {
    logger.info(`Service listening at http://localhost:${port}`);
  }
);

// start https if configured
if(isHttps){
  const API_HTTPS_CERT = process.env.API_HTTPS_CERT || false;
  const API_HTTPS_KEY = process.env.API_HTTPS_KEY || false;

  if(API_HTTPS_CERT && fs.existsSync(API_HTTPS_CERT) && API_HTTPS_KEY && fs.existsSync(API_HTTPS_KEY)){
    try {
      https.createServer({
        cert: fs.readFileSync(path.resolve(API_HTTPS_CERT)),
        key: fs.readFileSync(path.resolve(API_HTTPS_KEY))
      }, server).listen(process.env.API_HTTPS_PORT || 443);
    } catch (error) {
      logger.error("Error starting HTTPS service", error);
    }
  } else {
    logger.error("HTTPS Error: Cert and/or Cert Key Not Found. Skipping HTTPS");
  }
  
} else {
  logger.warn("HTTPS not configured.");
}

// refresh cors every CORS_REFRESH or 1 minute by default
setInterval(loadCORS, Number(process.env.CORS_REFRESH || 60000));

module.exports = app;