import express, { Express, Request, Response } from "express";
import { Authenticator } from 'passport';
import Web3 from "web3";
import { DateTime } from "luxon";

import os from 'os';
import fs from 'fs';
import path from 'path';

import { getBunyanLogger, WalletAccount, createTypedSignature, getWeb3, getEthereumConfig, loadWalletAccount, getHDWalletProvider } from "@playcheck/standard-lib";
import { LoadableService, MiddlewareChainMap, MiddlewareMap } from '../../types';
import Web3Strategy from "../../middlewares/passport-web3/strategy.jsx";
import { ACCOUNT_NOT_PERMITTED, addAdmin, createSessionTokenParams, getAdmins, INVALID_WALLET_ID, isAdmin, loadSessionCredential, LOGIN_EXPIRED, UNEXPECTED_ERROR } from "./index.jsx";

const logger = getBunyanLogger('web3-auth-api', { env: process.env.NODE_ENV });

const tmpdir = os.tmpdir();

// our eth system wallet's web3
const web3 = getWeb3( getEthereumConfig(process.env.MAIN_CHAIN_ACCOUNT || 'mainchain-dev') );

export const hotPlugFile = path.join(tmpdir, 'Web3AuthServiceDisabled');

export default class Web3AuthService implements LoadableService {
  
  constructor(...args:any[]){}

  serviceName() {
    return "Web3AuthService";
  }

  version() {
    return 1.0;
  }

  hotPlugControlFile(){
    return hotPlugFile;
  }

  public register(mainApp:Express, mountPath:string = "", middlewareChainMap:MiddlewareChainMap ,middlewares?:MiddlewareMap) {
    logger.info(`Service register`, this.serviceName(), mountPath);

    //find passport middleware
    if(middlewares && middlewares.passport && middlewares.passport instanceof Authenticator){
      const passport = middlewares.passport;

      const web3Auth = express();

      //set default response type to json
      web3Auth.use(function (req:Request, res:Response, next:Function) {
        res.header("Content-Type", 'application/json');
        next();
      });

      web3Auth.use(function (req:Request, res:Response, next:Function){
        if(fs.existsSync( hotPlugFile )){
          res.status(404).send({ message: 'Under Maintenance' });
        } else {
          next();
        }
      });

      //attach login strategy
      passport.use(new Web3Strategy( this.loginCallback, { passReqToCallback: true } ));

      passport.serializeUser(function(user:any, done) {
        done(null, user.ethWallet);
      });
       
      passport.deserializeUser(async (id:string, done) => {
        let retval = null;
        let error = null;

        await loadWalletAccount(new WalletAccount, id).then(account => {
          retval = account;
        }).catch(err => {
          error = err;
        });
        
        return done(error, retval);
      });

      web3Auth.post(`/login`, [passport.authenticate('web3')], middlewareChainMap.public, this.login);
      web3Auth.get(`/getHash`, middlewareChainMap.public, this.getUserSecret); //legacy
      web3Auth.get(`/loginHash`, middlewareChainMap.public, this.getUserSecret);

      mainApp.use(mountPath, web3Auth);

      middlewareChainMap.authenticated.push(this.authorizationFilter);
      middlewareChainMap.adminOnly.push(this.adminFilter);

      // populate admin list
      if(getAdmins().length === 0){
        const chainConfigs = [process.env.MAIN_CHAIN_ACCOUNT || 'mainchain-dev', process.env.SIDE_CHAIN_ACCOUNT || 'sidechain-dev', process.env.SIDE_CHAIN_PARTS_ACCOUNT || 'sidechain-dev-parts'];
        for (const chainConfig of chainConfigs){
          if(chainConfig){
            const wallet = getHDWalletProvider( chainConfig );
            addAdmin(...wallet.getAddresses());
          }
        }
      }

    } else {
      logger.error("Please provide the instance of passport middleware");
    }

    return false;
  }

  async loginCallback(req:Request, address:string, message:string, signed:string, done:Function){
    await loadWalletAccount(new WalletAccount, address).then(async account => {
      const userAccount = account;
      
      const sessionLength = Number(process.env.SESSION_TIMEOUT || 3600);
      const expiry = DateTime.now().plus({ seconds: sessionLength });

      //this is for compatibility with legacy API clients

      //generate token header for user
      const messageString = message.startsWith("0x") ? Web3.utils.hexToAscii(message) : message;

      const sessionTokenParams = createSessionTokenParams({
        address,
        signed,
        message: messageString
      });

      const sessionToken = await createTypedSignature(sessionTokenParams, web3.givenProvider || web3.currentProvider);

      if(sessionToken){
        userAccount.sessionTime = expiry.valueOf().toString();
        userAccount.signHash = messageString;
        userAccount.sessionToken = signed;
        userAccount.secretHash = userAccount.loginHash;

        await userAccount.save();

        req.body.serverSig = sessionToken;

      } else {
        logger.warn(`${address} login: session token resulted to false.`);
        
      }

      return done(null, userAccount);
      
    }).catch(err => {
      logger.error("Web3 Strategy Callback: Error retrieving wallet account!", err);
      //account does not exist on DB, but ok since the signature is valid
      return done(err);
    })
  }

  async login(req:Request, res:Response){
    const account = req.user as WalletAccount;

    /**
     * dev must send `${tokenId} ${ethWallet}` as value 
     * for authorization header for succeeding requests
     */
    res.send({
      address: account.ethWallet,
      server_sig: req.body.serverSig, //for compatibility with legacy apps
      tokenId: req.body.serverSig,
      sessionExpiry: account.sessionTime
    });
  }

  async getUserSecret(req:Request, res:Response){
    const { address } = req.query;
    const userAddress = Array.isArray(address) ? address[0] : address;

    if(!userAddress || userAddress === ""){
      res.status(500).send({ error: INVALID_WALLET_ID });
    } else {          
      const ethAddress = `${userAddress}`;

      await loadWalletAccount(new WalletAccount, ethAddress).then(async account => {
        const userAccount:WalletAccount = account;

        //generate secret hash
        userAccount.loginHash = Web3.utils.randomHex(32).replace('0x', '');

        await userAccount.save();
        
        res.send({message: 'Welcome to Battle Racers', date: (new Date()).toUTCString(),  hash: userAccount.loginHash});

      }).catch(err => {
        logger.error("Error retrieving wallet account", err);
        res.status(500).send({ error: UNEXPECTED_ERROR });
      });
    }
  }

  async authorizationFilter(req:Request, res:Response, next:Function){
    // logger.info(`Web3 Auth Filter`);

    try {
      const account = await loadSessionCredential(req, web3); 

      // logger.info(`Web3 Auth Filter`, account);

      if(!account){
        res.status(401).send({ error: LOGIN_EXPIRED });
      } else {
        // bump session expiry
        const sessionLength = Number(process.env.SESSION_TIMEOUT || 3600);
        const expiry = DateTime.now().plus({ seconds: sessionLength });
        
        account.sessionTime = expiry.valueOf().toString();
        await account.save();

        return next();
      }
    } catch (error) {
      logger.error(error);
      res.status(500).send(error);
    }
  }

  
  async adminFilter(req:Request, res:Response, next:Function){
    const account = await loadSessionCredential(req, web3);

    if(!account || !isAdmin(account.ethWallet)){
      res.status(403).send({ error: ACCOUNT_NOT_PERMITTED });
    } else {
      return next();
    }
  }
}