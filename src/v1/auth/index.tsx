import { Request } from "express";
import session from 'express-session';
import { MessageTypes, recoverTypedSignature, SignTypedDataVersion, TypedMessage } from "@metamask/eth-sig-util";
import { getBunyanLogger, loadWalletAccount, TokenId, WalletAccount } from "@playcheck/standard-lib";

import Web3 from "web3";
import { DateTime } from "luxon";

// type auto-merging for session vars
declare module 'express-session' {
  export interface SessionData {
    userAuthId: TokenId;
  }
}

const logger = getBunyanLogger('web3-auth-api', { env: process.env.NODE_ENV });

const adminList: string[] = [];

export const isAdmin = (userId: string) => {
  return adminList.includes(userId);
}

export const getAdmins = () => adminList;
export const addAdmin = (...userIds: string[]) => {
  userIds.forEach(userId => {
    if(!adminList.includes(userId)){
      adminList.push(userId);
    }
  });
}
export const delAdmin = (userId: string) => {
  const index  = adminList.indexOf(userId);

  if( index >= 0 ){
    adminList.splice(index, 1);
  }
}

//session EIP712 structure
const SessionDomain = [
  { name: "name", type: "string" },
  { name: "version", type: "string" }
];

const SessionMessage = [
  { name: "address", type: "address" },
  { name: "signed", type: "string" },
  { name: "message", type: "string" }
];

interface SessionParams {
  address: string;
  signed: string;
  message: string;
}

export const createSessionTokenParams = (sessionParams:SessionParams):TypedMessage<MessageTypes> => {
  return {
    types: {
      EIP712Domain: SessionDomain,
      Message: SessionMessage,
    },
    domain: {
      name: "Playcheck System API",
      version: "2.0"
    },
    primaryType: "Message",
    message: { ...sessionParams }
  }
}

//function to load user credential / profile
export const loadSessionCredential = async (req:Request, web3:Web3) => {
  logger.debug(`loading session credentials`, req.user);
  logger.debug(`loading session credentials headers`, req.headers);

  //load user account from session
  const reqUser:any = req.user;

  if(!reqUser){
    // user account does not exists in session
    // try loading it via header tokens, if submitted
    const { authorization } = req.headers;

    const has_authorization = !!authorization && authorization?.trim() !== "" && authorization?.split(' ').length === 2;

    if(!has_authorization){
      return false;
    }

    const [tokenId, ethWallet] = authorization.split(' ');
    const account = await loadWalletAccount(new WalletAccount, ethWallet);
    
    if(account && !!account.sessionToken && !!account.sessionTime && !!account.signHash){
      if(account.sessionTime < DateTime.now().valueOf().toString()){
        return false; //session expired
      }

      if(req.session && req.session?.userAuthId &&
        req.session?.userAuthId === tokenId){
          req.user = account;
          return account;
      }

      //form the session token params from profile
      const sessionTokenParams = createSessionTokenParams({
        address: ethWallet,
        signed: account.sessionToken,
        message: account.signHash
      });

      // validation requires that the recovered address must match the system's wallet
      // which was then encoded using signedType method
      const recovered = recoverTypedSignature({
        data: sessionTokenParams,
        signature: tokenId,
        version: SignTypedDataVersion.V4
      });

      if(recovered === (web3.givenProvider || web3.currentProvider).getAddress()){
        // save the token ID in session 
        // this means that we acknowledged 
        // this authorization for next requests
        if(req.session){
          const session = req.session;
          session.userAuthId = tokenId;
        };

        req.user = account;
        return account;
      }
    }

  } else {
    // user retrieved from session
    const account = await loadWalletAccount(new WalletAccount, reqUser.ethWallet);
    req.user = account;
    return account;
  }

  return false;
}

export const ACCOUNT_NOT_PERMITTED = "Not Permitted.";
export const LOGIN_EXPIRED = "Authrization Expired or Invalid.";
export const UNEXPECTED_ERROR = "Unexpected Error. Try Again Later.";
export const INVALID_WALLET_ID = "Invalid Wallet Address";