import express, { Express, Request, Response } from "express";

import os from 'os';
import fs from 'fs';
import path from 'path';

import { getBunyanLogger, isValidEthTransactionHash, WalletAccount } from "@playcheck/standard-lib";
import { LoadableService, MiddlewareChainMap, MiddlewareMap } from '../../types';
import { AccountCrateInfo, loadAccountByWallet, AccountCrateHistory } from "@playcheck/battle-racers";
import { DUPLICATE_TRANSACTION_HASH, INVALID_TRANSACTION_HASH, TRANSACTION_ALREADY_PROCESSED, TRANSACTION_CURRENTLY_PROCESSING, TRANSACTION_HASH_REQUIRED } from "./index.jsx";

const logger = getBunyanLogger('crate-opening-api', { env: process.env.NODE_ENV });

const tmpdir = os.tmpdir();

export const hotPlugFile = path.join(tmpdir, 'BRCrateServiceDisabled');

export default class CrateOpening implements LoadableService {
  
  constructor(...args:any[]){}

  serviceName() {
    return "BattleRacers/CrateService";
  }

  version() {
    return 1.0;
  }

  hotPlugControlFile(){
    return hotPlugFile;
  }

  public register(mainApp:Express, mountPath:string = "", middlewareChainMap:MiddlewareChainMap, middlewares?:MiddlewareMap) {
    logger.info(`Service register`, this.serviceName(), mountPath);

    const crateOpening = express();

    //set default response type to json
    crateOpening.use(function (req:Request, res:Response, next:Function) {
      res.header("Content-Type", 'application/json');
      next();
    });

    crateOpening.use(function (req:Request, res:Response, next:Function){
      if(fs.existsSync( hotPlugFile )){
        res.status(404).send({ message: 'Under Maintenance' });
      } else {
        next();
      }
    });

    crateOpening.post(`/openCrate`, middlewareChainMap.authenticated, this.addCrateOpeningTransaction);
    crateOpening.get(`/crateStatus`, middlewareChainMap.authenticated, this.pollMintedParts);
    crateOpening.post(`/clearStatus`, middlewareChainMap.authenticated, this.clearMintedParts);

    mainApp.use(mountPath, crateOpening);
  }

  async pollMintedParts(req:Request, res:Response){
    const account:AccountCrateInfo = await loadAccountByWallet(new AccountCrateInfo, (req.user as WalletAccount).ethWallet);

    const mintedParts = account.crateOpenedParts || {};
    const mintedIds = Object.keys(mintedParts);
    if(mintedIds.length > 0){
      for(let i = 0; i < mintedIds.length; i++){
        const mintId = mintedIds[i];
        if(mintedParts[mintId].done){
          return res.send({ parts: mintedParts[mintId].parts, showCaseId: mintId });
        }
      }
    }

    res.send({ parts: [] });
  }

  async clearMintedParts(req:Request, res:Response){
    const { txnId } = req.query;

    const txHash = txnId?.toString() || "";

    if(txHash && await isValidEthTransactionHash(txHash)){
      const account:AccountCrateInfo = await loadAccountByWallet(new AccountCrateInfo, (req.user as WalletAccount).ethWallet);

      account.crateOpenedParts = account.crateOpenedParts || {};
      if(!!account.crateOpenedParts[txHash]){
        delete(account.crateOpenedParts[txHash]);
        await account.save();
      }

      return res.send({ success: true });

    } else {
      return res.status(500).send({ error: TRANSACTION_HASH_REQUIRED, subject: ['txnId'] });

    }
  }

  async addCrateOpeningTransaction(req:Request, res:Response){
    const { tx, netId, crateType } = req.body;

    logger.debug(`Crate opening transaction submitted`, tx, netId, crateType);

    const account:AccountCrateHistory = await loadAccountByWallet(new AccountCrateHistory, (req.user as WalletAccount).ethWallet);
    account.pendingCrateOpenTxn = account.pendingCrateOpenTxn || {};

    const txHash = tx?.toString() || "";

    if(netId && crateType && !!txHash && await isValidEthTransactionHash(txHash, !!netId ? Number(netId) : undefined )){
      account.crateOpenedParts = account.crateOpenedParts || {};
      if(account.crateOpenedParts[txHash]){
        return res.status(400).send({ error: TRANSACTION_CURRENTLY_PROCESSING });
      }

      account.completedCrateOpenTxn = account.completedCrateOpenTxn || {};
      if(account.completedCrateOpenTxn[txHash]){
        return res.status(400).send({ error: TRANSACTION_ALREADY_PROCESSED, info: account.completedCrateOpenTxn[txHash] });
      }

      if(account.pendingCrateOpenTxn[txHash] === undefined){
        account.pendingCrateOpenTxn[txHash] = {
          networkId: netId?.toString(),
          crateBox: crateType?.toString(),
          postedAt: (new Date()).valueOf()
        }; 

        //save tx hash to be processed later
        await account.save();

        return res.send({ success: true, tx: {
          ...account.pendingCrateOpenTxn[txHash],
          transactionHash: tx,
          validated: !!netId
        } });
      }

      return res.status(400).send({ error: DUPLICATE_TRANSACTION_HASH });

    } else {
      return res.status(400).send({ error: INVALID_TRANSACTION_HASH });

    }
  }
}
