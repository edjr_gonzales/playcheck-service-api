import express, { Express, Request, Response } from "express";
import Web3 from 'web3';

import os from 'os';
import fs from 'fs';
import path from 'path';

import { getBunyanLogger, isNumeric, WalletAccount, getEthereumConfig, EthereumAddress, getWeb3, Wallet, isValidEthTransactionHash, queryMappedClass, queryMappedItem } from "@playcheck/standard-lib";
import { ScrapReward, BRPartMetadata, loadAccountByWallet, loadMetadataById, Scrap, RaceToken, ScrapRewardClaim, ScrapPartUpgrade, ScrapRaceTokenPurchase, RaceTokenEventPassPurchase, BRAccount } from "@playcheck/battle-racers";
import { LoadableService, MiddlewareChainMap, MiddlewareMap } from '../../types';
import { isAdmin } from "../auth/index.jsx";
import { ExpressionAttributeValueMap } from "aws-sdk/clients/dynamodb";
import { getEventData, makeClaimRewardStub } from "./index.jsx";

const logger = getBunyanLogger('scrap-api', { env: process.env.NODE_ENV });

const tmpdir = os.tmpdir();

export const hotPlugFile = path.join(tmpdir, 'BRScrapServiceDisabled');

const UPGRADE_BASE_PRICE = Number(process.env.PART_UPGRADE_SCRAP_PRICE || '60');
const UPGRADE_PRICE_MULTIPLIER = Number(process.env.PART_UPGRADE_SCRAP_MTPLIER || '1.2');
const RACE_TOKEN_PRICE = Number(process.env.RACE_TOKEN_SCRAP_PRICE || '1');

const DUMMY_TYPE_SCRAP_ORDER = 1
const PART_UPGRADE_SCRAP_ORDER = 2
const EVENT_PASS_RACETKN_ORDER = 4
const RACE_TOKEN_SCRAP_ORDER = 8

export default class ScrapService implements LoadableService {
  
  constructor(...args:any[]){}

  serviceName() {
    return "BattleRacers/ScrapService";
  }

  version() {
    return 1.0;
  }

  hotPlugControlFile(){
    return hotPlugFile;
  }

  public register(mainApp:Express, mountPath:string = "", middlewareChainMap:MiddlewareChainMap, middlewares?:MiddlewareMap) {
    logger.info(`Service register`, this.serviceName(), mountPath);

    const serviceApi = express();

    //set default response type to json
    serviceApi.use(function (req:Request, res:Response, next:Function) {
      res.header("Content-Type", 'application/json');
      next();
    });

    serviceApi.use(function (req:Request, res:Response, next:Function){
      if(fs.existsSync( hotPlugFile )){
        res.status(404).send({ message: 'Under Maintenance' });
      } else {
        next();
      }
    });

    serviceApi.post(`/createReward`, middlewareChainMap.authenticated, middlewareChainMap.adminOnly, this.createReward);
    serviceApi.post(`/claimReward`, middlewareChainMap.authenticated, this.claimReward);
    serviceApi.post(`/setClaimTxn/:claimId`, middlewareChainMap.authenticated, this.setClaimRewardTxHash);
    serviceApi.get(`/listPendingRewards`, middlewareChainMap.authenticated, this.listReward);
    serviceApi.get(`/listInProgressRewards`, middlewareChainMap.authenticated, this.listOnClaimReward);

    serviceApi.patch(`/upgradePart/:partId`, middlewareChainMap.authenticated, this.upgradePart);
    serviceApi.post(`/buyEventPass`, middlewareChainMap.authenticated, this.buyEventPass);
    serviceApi.post(`/buyRaceToken`, middlewareChainMap.authenticated, this.buyRaceToken);

    mainApp.use(mountPath, serviceApi);
  }

  async createReward(req:Request, res:Response){
    const loggedUser = req.user as WalletAccount;
    
    try {
      const { recipient, amount } = req.body;
      const walletAddr = new EthereumAddress(recipient);

      logger.debug(`creating reward for `, recipient, amount);
      // logger.debug(`reward granted by `, loggedUser);

      if(recipient && walletAddr.$ && isNumeric(amount) && Number(amount) >= 1){
        const rewardId = Web3.utils.randomHex(16);
        const requestTime = Date.now();

        const claimSignatureStub = await makeClaimRewardStub(rewardId, walletAddr, amount);
        if(!claimSignatureStub){
          return res.status(500).send({ error: 'Unable to create claim signature stub.' });
        }

        const rewardData = Object.assign(new ScrapReward, { 
          scrapDataKey: `${recipient}#ScrapRewards`,
          scrapDataId: `Reward#${recipient}#${requestTime}#${rewardId}`,
          ethWallet: recipient,
          amount,
          rewardId,
          claimId: undefined,
          grantedBy: loggedUser.ethWallet,
          grantedOn: requestTime,
          claimStub: claimSignatureStub
        });

        await rewardData.save();

        res.send({ success: true, reward: rewardData });
      } else {
        res.status(500).send({ error: 'recipient and/or amount is invalid' });
      }
    } catch (error) {
      logger.error(error);
      res.status(500).send({ error });
    }
  }

  async listReward(req:Request, res:Response){
    const loggedUser = req.user as WalletAccount;
    const rewardDataList: ScrapReward[] = [];
    
    for await (const rewardData of queryMappedClass({
      /** Using Primary Key+Hash */
      KeyConditionExpression: `#keyName = :keyValue AND begins_with(#sortName, :sortPrefix)`,
      FilterExpression: "attribute_not_exists(#A0001)",
      ExpressionAttributeNames: {
        '#keyName': 'scrapDataKey',
        '#sortName': 'scrapDataId',
        '#A0001': 'claimId'
      },
      ExpressionAttributeValues: {
        ':keyValue': `${loggedUser.ethWallet}#ScrapRewards`,
        ':sortPrefix': 'Reward#',
      },
      
    }, ScrapReward)) {
      rewardDataList.push(rewardData);
    }
    res.send({ success: true, rewards: rewardDataList });
    
  }

  // authenticated
  async claimReward(req:Request, res:Response){
    const { recipient, rewards } = req.body;

    const loggedUser = req.user as WalletAccount;
    const walletAddr = new EthereumAddress(recipient);

    // can be operated by the reward recipient or an admin
    if( recipient && walletAddr && ( walletAddr.equals(loggedUser.ethWallet) || isAdmin(loggedUser.ethWallet) ) ){
      if((Array.isArray(rewards) && rewards.length > 0)){
        const targetRewards: ScrapReward[] = [];

        const filtered = rewards.filter(reward => !!reward && reward !== '');
        const valueMap: string[] = filtered.map((reward, i) => `:B000${i}`);
        const attrVals:ExpressionAttributeValueMap = {};

        valueMap.forEach((v, i) => {
          attrVals[v] = filtered[i]
        });

        for await (const rewardData of queryMappedClass({
          /** Using Primary Key+Hash */
          KeyConditionExpression: `#keyName = :keyValue AND begins_with(#sortName, :sortPrefix)`,
          FilterExpression: `#A0001 IN (${valueMap.join(', ')}) AND attribute_not_exists(#A0002)`,
          ExpressionAttributeNames: {
            '#keyName': 'scrapDataKey',
            '#sortName': 'scrapDataId',
            '#A0001': 'rewardId',
            '#A0002': 'claimId',
          },
          ExpressionAttributeValues: {
            ':keyValue': `${loggedUser.ethWallet}#ScrapRewards`,
            ':sortPrefix': 'Reward#',
            ...attrVals
          },
          
        }, ScrapReward)) {
          targetRewards.push(rewardData);
        }

        // gather rewards that wanted to be claimed
        const validRewardIds = targetRewards.map(rewardData => rewardData.rewardId);
        let totalRewards = 0;
        
        targetRewards.forEach(rewardData => {
          // TODO Validate Signature rewardData.claimStub     
          totalRewards += Number(rewardData.amount);
        });

        // generate real reward ID
        const claimId = Web3.utils.randomHex(16);
        const claimSignature = await makeClaimRewardStub(claimId, walletAddr, totalRewards);

        if(claimSignature){
          const requestTime = Date.now();

          const claimReward = Object.assign(new ScrapRewardClaim, {
            scrapDataKey: `${recipient}#ScrapClaims`,
            scrapDataId: `ClaimReward#${recipient}#${requestTime}#${claimId}`,
            ethWallet: recipient,
            claimId,
            rewards: validRewardIds,
            claimData: claimSignature,
            claimedOn: requestTime,
            tx: ''
          });

          await claimReward.save();

          for await (const validatedReward of targetRewards){
            validatedReward.claimId = claimId;
            await validatedReward.save();
          }

          res.send({ signature: claimSignature, rewardId: claimId, rewards: validRewardIds, amount: totalRewards });
        } else {
          res.status(500).send({ message: 'Please try again later.' })
        }
      } else {
        res.status(500).send({ error: 'rewards to be claimed are not specified' });
      }
      
    } else {
      res.status(500).send({ error: 'recipient is invalid' });
    }
  }

  async listOnClaimReward(req:Request, res:Response){
    const loggedUser = req.user as WalletAccount;
    const rewardClaimList: ScrapRewardClaim[] = [];

    for await (const rewardData of queryMappedClass({
      /** Using Primary Key+Hash */
      KeyConditionExpression: `#keyName = :keyValue AND begins_with(#sortName, :sortPrefix)`,
      FilterExpression: "attribute_not_exists(#A0001)",
      ExpressionAttributeNames: {
        '#keyName': 'scrapDataKey',
        '#sortName': 'scrapDataId',
        '#A0001': 'receipt'
      },
      ExpressionAttributeValues: {
        ':keyValue': { S: `${loggedUser.ethWallet}#ScrapClaims` },
        ':sortPrefix': { S: 'ClaimReward#' },
      },
      
    }, ScrapRewardClaim)) {
      rewardClaimList.push(rewardData);
    }

    res.send({ success: true, list: rewardClaimList.map(rewardClaimData => ({
        claimId: rewardClaimData.claimId,
        tx: rewardClaimData.tx,
        claimData: rewardClaimData.claimData,
        claimedOn: rewardClaimData.claimedOn,
        status: rewardClaimData.status
      }))
    });
  }

  async setClaimRewardTxHash(req:Request, res:Response) {
    const { txHash } = req.body;
    const { claimId } = req.params;

    if(!claimId || !txHash){
      return res.status(404).send({ error: `One ore more parameter(s) not found. Expected txHash, claimId` });
    }

    const loggedUser = req.user as WalletAccount;
    const targetClaimRewardData = await queryMappedItem({
      /** Using Primary Key+Hash */
      KeyConditionExpression: `#keyName = :keyValue AND begins_with(#sortName, :sortPrefix)`,
      FilterExpression: "#A0001 = :A0001",
      ExpressionAttributeNames: {
        '#keyName': 'scrapDataKey',
        '#sortName': 'scrapDataId',
        '#A0001': 'claimId'
      },
      ExpressionAttributeValues: {
        ':keyValue': { S: `${loggedUser.ethWallet}#ScrapClaims` },
        ':sortPrefix': { S: 'ClaimReward#' },
        ':A0001': { S: claimId }
      },
      
    }, ScrapRewardClaim);

    if(targetClaimRewardData){
      targetClaimRewardData.tx = txHash;
      targetClaimRewardData.txOn = Date.now();
      await targetClaimRewardData.save();

      res.send({ success: 'Reward claim transaction hash set.' });
    } else {
      res.status(404).send({ error: 'Reward claim ID not found.' });
    }
  }

  // get orders
  /**
   * `orderType` is OR'ed value that determines value to get
   *  2 for parts upgrade orders
   *  4 for event pass orders
   *  8 for race token orders
   * 
   * OR combinations
   *  6  for part upgrades and event pass orders ((2 + 4) = 6)
   *  10 for part upgrades and race token orders ((2 + 8) = 10)
   *  12 for event pass and race token orders ((4 + 8) = 12)
   *  14 for all ((2 + 4 + 8) = 14)
   * @param req 
   * @param res 
   */
  async getOrders(req:Request, res:Response){
    const { orderType, ethAccount, status } = req.params;

    const loggedUser = req.user as WalletAccount;

    const returnData:any = {};
    const orderTypeId = Number(orderType || 14);

    const getOrderOf = !!ethAccount ? ethAccount : loggedUser.ethWallet;
    const orderStatus = status || 'all';
    
    if((orderTypeId & PART_UPGRADE_SCRAP_ORDER) === PART_UPGRADE_SCRAP_ORDER){
      returnData.upgrades = {};

      for await (const scrapData of queryMappedClass({
        /** Using Primary Key+Hash */
        KeyConditionExpression: `#keyName = :keyValue AND begins_with(#sortName, :sortPrefix)`,
        FilterExpression: orderStatus !== 'all' ? "#A0001 = :A0001" : undefined,
        ExpressionAttributeNames: {
          '#keyName': 'scrapDataKey',
          '#sortName': 'scrapDataId',
          '#A0001': 'orderStatus'
        },
        ExpressionAttributeValues: {
          ':keyValue': { S: `${getOrderOf}#PartUpgrades` },
          ':sortPrefix': { S: 'PartUpgrade#' },
          ':A0001': { S: orderStatus },
        },
        
      }, ScrapPartUpgrade)){
        returnData.upgrades[scrapData.orderId] = {
          fee: scrapData.fee,
          value: scrapData.value,
          qty: scrapData.qty,
          tx: scrapData.tx,
          orderStatus: scrapData.orderStatus,
          payer: scrapData.payer,
          recipient: scrapData.recipient
        }
      }
    }

    if((orderTypeId & EVENT_PASS_RACETKN_ORDER) === EVENT_PASS_RACETKN_ORDER){
      returnData.eventPasses = {};

      for await (const raceTokenData of queryMappedClass({
        /** Using Primary Key+Hash */
        KeyConditionExpression: `#keyName = :keyValue AND begins_with(#sortName, :sortPrefix)`,
        FilterExpression: orderStatus !== 'all' ? "#A0001 = :A0001" : undefined,
        ExpressionAttributeNames: {
          '#keyName': 'raceTokenDataKey',
          '#sortName': 'raceTokenDataId',
          '#A0001': 'orderStatus'
        },
        ExpressionAttributeValues: {
          ':keyValue': { S: `${getOrderOf}#PartUpgrades` },
          ':sortPrefix': { S: 'PartUpgrade#' },
          ':A0001': { S: orderStatus },
        },
        
      }, RaceTokenEventPassPurchase)){
        returnData.raceTokenPurchases[raceTokenData.orderId] = {
          fee: raceTokenData.fee,
          value: raceTokenData.value,
          qty: raceTokenData.qty,
          tx: raceTokenData.tx,
          orderStatus: raceTokenData.orderStatus,
          payer: raceTokenData.payer,
          recipient: raceTokenData.recipient
        }
      }
    }

    if((orderTypeId & RACE_TOKEN_SCRAP_ORDER) === RACE_TOKEN_SCRAP_ORDER){
      returnData.raceTokenPurchases = {};

      for await (const scrapData of queryMappedClass({
        /** Using Primary Key+Hash */
        KeyConditionExpression: `#keyName = :keyValue AND begins_with(#sortName, :sortPrefix)`,
        FilterExpression: orderStatus !== 'all' ? "#A0001 = :A0001" : undefined,
        ExpressionAttributeNames: {
          '#keyName': 'scrapDataKey',
          '#sortName': 'scrapDataId',
          '#A0001': 'orderStatus'
        },
        ExpressionAttributeValues: {
          ':keyValue': { S: `${getOrderOf}#PartUpgrades` },
          ':sortPrefix': { S: 'PartUpgrade#' },
          ':A0001': { S: orderStatus },
        },
        
      }, ScrapRaceTokenPurchase)){
        returnData.raceTokenPurchases[scrapData.orderId] = {
          fee: scrapData.fee,
          value: scrapData.value,
          qty: scrapData.qty,
          tx: scrapData.tx,
          orderStatus: scrapData.orderStatus,
          payer: scrapData.payer,
          recipient: scrapData.recipient
        }
      }
    }

    res.send(returnData);
  }

  // get order
  async getOrder(req:Request, res:Response){
    const { orderId, orderType, ethAccount } = req.params;

    if(!orderId){
      return res.status(400).send({ error: 'Parameter orderId not found.' });
    }

    const loggedUser = req.user as WalletAccount;
    
    const orderTypeId = Number(orderType || 14);
    const getOrderOf = !!ethAccount ? ethAccount : loggedUser.ethWallet;

    let returnData:any = {};

    if((orderTypeId & PART_UPGRADE_SCRAP_ORDER) === PART_UPGRADE_SCRAP_ORDER){
      
      const scrapData = await queryMappedItem({
        /** Using Primary Key+Hash */
        KeyConditionExpression: `#keyName = :keyValue AND begins_with(#sortName, :sortPrefix)`,
        FilterExpression: `#A0001 = :A0001`,
        ExpressionAttributeNames: {
          '#keyName': 'scrapDataKey',
          '#sortName': 'scrapDataId',
          '#A0001': 'orderId'
        },
        ExpressionAttributeValues: {
          ':keyValue': { S: `${getOrderOf}#PartUpgrades` },
          ':sortPrefix': { S: 'PartUpgrade#' },
          ':A0001': { S: orderId },
        },
        
      }, ScrapPartUpgrade);

      if(scrapData){
        const { scrapDataKey, scrapDataId, ...rest } = scrapData;
        returnData = { ...rest };
      }
      
    } else if((orderTypeId & RACE_TOKEN_SCRAP_ORDER) === RACE_TOKEN_SCRAP_ORDER){
      
      const scrapData = await queryMappedItem({
        /** Using Primary Key+Hash */
        KeyConditionExpression: `#keyName = :keyValue AND begins_with(#sortName, :sortPrefix)`,
        FilterExpression: `#A0001 = :A0001`,
        ExpressionAttributeNames: {
          '#keyName': 'scrapDataKey',
          '#sortName': 'scrapDataId',
          '#A0001': 'orderId'
        },
        ExpressionAttributeValues: {
          ':keyValue': { S: `${getOrderOf}#RaceTokenPurchases` },
          ':sortPrefix': { S: 'BuyRaceToken#' },
          ':A0001': { S: orderId },
        },
        
      }, ScrapRaceTokenPurchase);

      if(scrapData){
        const { scrapDataKey, scrapDataId, ...rest } = scrapData;
        returnData = { ...rest };
      }

    } else if((orderTypeId & EVENT_PASS_RACETKN_ORDER) === EVENT_PASS_RACETKN_ORDER){
      
      const tokenRaceData = await queryMappedItem({
        /** Using Primary Key+Hash */
        KeyConditionExpression: `#keyName = :keyValue AND begins_with(#sortName, :sortPrefix)`,
        FilterExpression: `#A0001 = :A0001`,
        ExpressionAttributeNames: {
          '#keyName': 'raceTokenDataKey',
          '#sortName': 'raceTokenDataId',
          '#A0001': 'orderId'
        },
        ExpressionAttributeValues: {
          ':keyValue': { S: `${getOrderOf}#EventPassPurchases` },
          ':sortPrefix': { S: 'BuyEventPass#' },
          ':A0001': { S: orderId },
        },
        
      }, RaceTokenEventPassPurchase);

      if(tokenRaceData){
        const { raceTokenDataKey, raceTokenDataId, ...rest } = tokenRaceData;
        returnData = { ...rest };
      }
    }

    res.send({ [orderId]: returnData });
  }

  // upgradePart
  async upgradePart(req:Request, res:Response){
    const { partId } = req.params;

    if(!partId){
      return res.status(400).send({ error: 'Parameter partId not found.' });
    }

    const loggedUser = req.user as WalletAccount;
    const upgraderWallet = new Wallet(loggedUser.ethWallet);
    const upgraderAccount = await loadAccountByWallet(new BRAccount, loggedUser.ethWallet);

    const partMetadata = await loadMetadataById(new BRPartMetadata, partId);

    if(partMetadata.getLevel() < partMetadata.getMaxLevel()){
      const configName = process.env.SIDE_CHAIN_ACCOUNT || 'sidechain-dev';
      const config = getEthereumConfig( configName );

      // load scrap contract
      const scrap = new Scrap( config );

      // this checks the balance of upgrader, not necessarily the owner
      const upgraderBal = await scrap.balanceOf(new Wallet(upgraderAccount.ethWallet));
      const upgradePrice = Math.round( UPGRADE_BASE_PRICE * UPGRADE_PRICE_MULTIPLIER );

      if( upgraderBal >= upgradePrice ){
        const orderId = `UP#${Date.now()}#${partId}#${partMetadata.getLevel()}`;

        // deduct the scrap balance and obtain the transaction hash to be checked later
        const txHash:any = await scrap.withdrawOrderPayment(
          upgraderWallet, 
          scrap.web3.utils.asciiToHex(orderId), 
          upgradePrice, 
          {
            'transactionHash': (txHash:string) => {
              return txHash;
            }
          });

        // save to the pending list for later processing
        logger.info(`Part upgrade request ${orderId} from ${upgraderWallet.$} transaction hash`, txHash);
        if(typeof txHash === 'string' && txHash !== '' && isValidEthTransactionHash(txHash)){

          const upgradeOrder = Object.assign(new ScrapPartUpgrade(),{
            scrapDataKey: `${upgraderWallet.$}#PartUpgrades`,
            scrapDataId: `PartUpgrade#${Date.now()}#${upgraderWallet.$}#${orderId}`,
            orderId: orderId,
            fee: upgradePrice,
            value: partId,
            qty: 1,
            payer: upgraderWallet.$,
            recipient: partMetadata.owner,
            tx: txHash,
            txOn: Date.now(),
            orderStatus: 'new'});

          await upgradeOrder.save();

          const { scrapDataKey, scrapDataId, ...upgradeOrderData } = upgradeOrder;

          return res.send({ [orderId]: upgradeOrderData });

        } else {
          logger.error(`Unexpected transaction order result`, txHash);
          return res.status(400).send({ error: `Unexpected transaction order result`, txHash });
        }
      } else {
        return res.status(400).send({ error: `Not enough ${await scrap.tokenSymbol()} balance.` });
      }
    } else {
      return res.status(400).send({ error: 'Part already reached max level.' });
    }
  }

  // buy event pass
  async buyEventPass(req:Request, res:Response){
    const { eventId, recipient } = req.body;

    if(!eventId){
      return res.status(400).send({ error: 'Parameter eventId not found.' });
    }

    const loggedUser = req.user as WalletAccount;
    const buyerWallet = new Wallet(loggedUser.ethWallet);

    const recipientWallet = !!recipient ? new Wallet(recipient) : buyerWallet;

    const event = await getEventData(eventId);

    const configName = process.env.SIDE_CHAIN_ACCOUNT || 'sidechain-dev';
    const config = getEthereumConfig( configName );

    // load scrap contract
    const raceToken = new RaceToken( config );

    // this checks the balance of upgrader, not necessarily the owner
    const buyerBal = await raceToken.balanceOf(new Wallet(buyerWallet.$));

    if( buyerBal >= event.price ){
      const orderId = `EVTP#${Date.now()}#${eventId}#${recipientWallet.$}`;

      // deduct the scrap balance and obtain the transaction hash to be checked later
      // TODO test this theory
      const txHash:any = await raceToken.withdrawOrderPayment(
        buyerWallet, 
        raceToken.web3.utils.asciiToHex(orderId), 
        event.price, 
        {
          'transactionHash': (txHash:string) => {
            return txHash;
          }
        });

      // save to the pending list for later processing
      logger.info(`Event pass order ${orderId} from ${buyerWallet.$} transaction hash`, txHash);
      if(typeof txHash === 'string' && txHash !== '' && isValidEthTransactionHash(txHash)){
        
        const eventPassOrder = Object.assign(new RaceTokenEventPassPurchase(),{
          tokenRaceDataKey: `${buyerWallet.$}#EventPassPurchases`,
          tokenRaceDataId: `BuyEventPass#${Date.now()}#${buyerWallet.$}#${orderId}`,
          orderId: orderId,
          fee: event.price,
          value: eventId,
          qty: 1,
          payer: buyerWallet.$,
          recipient: recipientWallet.$,
          tx: txHash,
          txOn: Date.now(),
          orderStatus: 'new'});

        await eventPassOrder.save();

        const { tokenRaceDataKey, tokenRaceDataId, ...eventPassOrderData } = eventPassOrder;

          return res.send({ [orderId]: eventPassOrderData });

      } else {
        logger.error(`Unexpected transaction order result`, txHash);
        return res.status(400).send({ error: `Unexpected transaction order result`, txHash });
      }
    } else {
      return res.status(400).send({ error: `Not enough ${await raceToken.tokenSymbol()} balance.` });
    }
  }

  // buy race token
  async buyRaceToken(req:Request, res:Response){
    const { amount, recipient } = req.body;

    if(!amount){
      return res.status(400).send({ error: 'Parameter amount not found.' });
    }

    const raceTokenQty = Number(amount);

    const loggedUser = req.user as WalletAccount;
    const buyerWallet = new Wallet(loggedUser.ethWallet);

    const recipientWallet = !!recipient ? new Wallet(recipient) : buyerWallet;

    const configName = process.env.SIDE_CHAIN_ACCOUNT || 'sidechain-dev';
    const config = getEthereumConfig( configName );

    // load scrap contract
    const scrap = new Scrap( config );

    // this checks the balance of upgrader, not necessarily the owner
    const upgraderBal = await scrap.balanceOf(new Wallet(buyerWallet.$));
    const purchasePrice = Math.round( raceTokenQty * RACE_TOKEN_PRICE );
    
    if( upgraderBal >= purchasePrice ){
      const orderId = `RTKN#${Date.now()}#${buyerWallet.$}/${recipientWallet.$}#${raceTokenQty}`;

      // deduct the scrap balance and obtain the transaction hash to be checked later
      // TODO test this theory
      const txHash:any = await scrap.withdrawOrderPayment(
        buyerWallet, 
        scrap.web3.utils.asciiToHex(orderId), 
        purchasePrice, 
        {
          'transactionHash': (txHash:string) => {
            return txHash;
          }
        });

      // save to the pending list for later processing
      logger.info(`Race token order for ${recipientWallet.$} with the amount of ${raceTokenQty} transaction hash`, txHash);
      if(typeof txHash === 'string' && txHash !== '' && isValidEthTransactionHash(txHash)){

        const raceTokenOrder = Object.assign(new ScrapRaceTokenPurchase(),{
          scrapDataKey: `${buyerWallet.$}#RaceTokenPurchases`,
          scrapDataId: `BuyRaceToken#${Date.now()}#${buyerWallet.$}#${orderId}`,
          orderId: orderId,
          fee: purchasePrice,
          value: raceTokenQty,
          qty: raceTokenQty,
          payer: buyerWallet.$,
          recipient: recipientWallet.$,
          tx: txHash,
          txOn: Date.now(),
          orderStatus: 'new'});

        await raceTokenOrder.save();

        const { scrapDataKey, scrapDataId, ...raceTokenOrderData } = raceTokenOrder;

        return res.send({ [orderId]: raceTokenOrderData });

      } else {
        logger.error(`Unexpected transaction order result`, txHash);
        return res.status(400).send({ error: `Unexpected transaction order result`, txHash });
      }
    } else {
      return res.status(400).send({ error: `Not enough ${await scrap.tokenSymbol()} balance.` });
    }
  }
}

