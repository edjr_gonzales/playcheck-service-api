import express, { Express, Request, Response } from "express";

import os from 'os';
import fs from 'fs';
import path from 'path';

import { getBunyanLogger, isNumeric, Address, getWeb3, getEthereumConfig, scanMappedClass } from "@playcheck/standard-lib";
import { LoadableService, MiddlewareChainMap, MiddlewareMap } from '../../types';
import { BRPartMetadata, formatMetadata, CRATE_LIST, BRMetadataType, BRPartSystemMetadata, PART_STATE_CLAIMING, PART_STATE_ETH_AWAIT_CLAIM, PART_STATE_ETH_CLAIM_READY, PART_STATE_TRANSIT, PART_STATE_READY } from "@playcheck/battle-racers";
import { loadSessionCredential } from "../auth/index.jsx";

const logger = getBunyanLogger('metadata-api', { env: process.env.NODE_ENV });

const tmpdir = os.tmpdir();

export const hotPlugFile = path.join(tmpdir, 'BRMetadataServiceDisabled');

const publicMetadataAttr = [
  'id', 'name', 'owner', 'description',
  'image', 'external_url', 'details',
  'provisioningId', 'crateDrawTx', 'crateType'
];

export default class BRMetadata implements LoadableService {
  
  constructor(...args:any[]){}

  serviceName() {
    return "BattleRacers/MetadataService";
  }

  version() {
    return 1.0;
  }

  hotPlugControlFile(){
    return hotPlugFile;
  }

  public register(mainApp:Express, mountPath:string = "", middlewareChainMap:MiddlewareChainMap, middlewares?:MiddlewareMap) {
    logger.info(`Service register`, this.serviceName(), mountPath);

    const metadataSvc = express();

    //set default response type to json
    metadataSvc.use(function (req:Request, res:Response, next:Function) {
      res.header("Content-Type", 'application/json');
      next();
    });

    metadataSvc.use(function (req:Request, res:Response, next:Function){
      if(fs.existsSync( hotPlugFile )){
        res.status(404).send({ message: 'Under Maintenance' });
      } else {
        next();
      }
    });

    // complete metadata
    metadataSvc.get(`/part/:id`, middlewareChainMap.public, this.partMetadata);
    metadataSvc.get(`/userParts`, middlewareChainMap.public, this.userParts); 

    // backward compatibility
    mainApp.get('/api/getParts', async (req:Request, res:Response) => {
      res.redirect(`${mountPath}/userParts`);
    });

    //opensea formatted
    metadataSvc.get(`/items/:id`, middlewareChainMap.public, this.openseaPartMetadata);
    metadataSvc.get(`/crate/:address`, middlewareChainMap.public, this.crateMetadata);

    mainApp.get('/crates/:address', async (req:Request, res:Response) => {
      res.redirect(`${mountPath}/crate/${req.params.address}`);
    });

    mainApp.get('/api/items/:id', async (req:Request, res:Response) => {
      res.redirect(`${mountPath}/items/${req.params.id}`);
    });

    mainApp.use(mountPath, metadataSvc);
  }

  async userParts(req:Request, res:Response){
    const { address, page, pageSize } = req.params;

    const web3 = getWeb3( getEthereumConfig(process.env.MAIN_CHAIN_ACCOUNT || 'mainchain-dev') );
    const isAutheticated = await loadSessionCredential(req, web3);

    // query user parts
    const userParts:BRMetadataType[] = [];

    const partIterator = await scanMappedClass({
      FilterExpression: '#00001 = :00001',
      ExpressionAttributeNames: {
        '#00001': 'owner',
      },
      ExpressionAttributeValues: {
        ':00001': address as any
      }
    }, isAutheticated ? BRPartSystemMetadata : BRPartMetadata);

    const pageNumber = Number( isNumeric(page) ? page : '1');
    const pageCount =  Number( isNumeric(pageSize) ? pageSize: '1000');

    const startIndex = (pageNumber - 1) * pageCount;
    const endIndex = pageNumber * pageCount;

    const claiming:BRMetadataType[] = [];
    const readyToClaim:BRMetadataType[] = [];
    const pendingClaim:BRMetadataType[] = [];

    let itemIndex = 0;
    let total = 0;
    let hasNext = false;
    for await(const part of partIterator){
      total++;

      if(isAutheticated && (part as BRPartSystemMetadata).state === PART_STATE_CLAIMING){
        claiming.push(part);

      } else if(isAutheticated && (part as BRPartSystemMetadata).state === PART_STATE_ETH_AWAIT_CLAIM){
        pendingClaim.push(part);

      } else if(isAutheticated && (part as BRPartSystemMetadata).state === PART_STATE_ETH_CLAIM_READY){
        readyToClaim.push(part);

      } else if((part as BRPartSystemMetadata).state === PART_STATE_READY) {
        if(itemIndex >= startIndex && itemIndex < endIndex){
          userParts.push(part);
          itemIndex++;

        } else if(itemIndex === endIndex) {
          hasNext = true;
  
        } else {
          // break; // dont break for the sake of sorting on state
        }
      }
    }

    res.send({
      page: pageNumber,
      size: pageCount,
      parts: userParts,
      next: hasNext,
      claiming, 
      readyToClaim, 
      pendingClaim,
      total
    });
  }

  async partMetadata(req:Request, res:Response){
    const { id } = req.params;

    if(id && isNumeric(id)){
      const metadata = Object.assign(new BRPartMetadata, { id });
      const result = await metadata.load();

      if(!result){
        return res.status(404).send({error: "Metadata ID Not Found"});
      } else {
        // filter public data out
        const publicMetadata:any = {};

        for(const prop in metadata){
          if ( publicMetadataAttr.includes(prop) ) {
            publicMetadata[prop] = (metadata as any)[prop];
          }
        }

        res.send(publicMetadata);
      }

    } else {
      return res.status(500).send({error: "Metadata ID Required"});
    }
  }

  async openseaPartMetadata(req:Request, res:Response){
    const { id } = req.params;

    if(id && isNumeric(id)){
      const metadata = Object.assign(new BRPartMetadata, { id });
      const result = await metadata.load();

      if(!result){
        return res.status(404).send({error: "Metadata ID Not Found"});
      } else {
        res.send(formatMetadata(metadata));
      }

    } else {
      return res.status(500).send({error: "Metadata ID Required"});
    }
  }

  async crateMetadata(req:Request, res:Response){
    const { address } = req.params;

    if(address && Address(address)){
      logger.info(`Crate Metadata By Address: ${address}`);

      // private attributes, not to be exposed to public
      // const privateAttr = ["contractFile", "contractSym", "contractName", "icon"];

      const searchPath = [
        path.resolve(process.cwd(), path.join('.', `crates-metadata.json`)),
        path.resolve(process.cwd(), path.join('.', 'public', `crates-metadata.json`)),
        path.resolve(process.cwd(), path.join('.', 'build', `crates-metadata.json`)),
      ];

      if(process.env.CRATE_METADATA_JSON_PATH) {
        searchPath.push(path.resolve(process.env.CRATE_METADATA_JSON_PATH, `crates-metadata.json`));
        searchPath.push(path.resolve(process.cwd(), path.join(process.env.CRATE_METADATA_JSON_PATH, `crates-metadata.json`)));
      }

      let crateMetadata = null;

      for(let i = 0; i < searchPath.length; i++){
        const jsonFilePath = searchPath[i];

        logger.info(`Crate Metadata By Address: Loading JSON from ${jsonFilePath}`);

        if(fs.existsSync(jsonFilePath)){
          const contents = fs.readFileSync(jsonFilePath, 'utf8');

          logger.info(`Crate Metadata By Address: Loaded JSON from ${jsonFilePath}`);

          try {
            crateMetadata = JSON.parse(contents);
            break;
          } catch(err) {
            logger.info(`Crate Metadata By Address: JSON parsing error ${jsonFilePath} ${err}`);
          }
        }
      }

      if(crateMetadata){
        //let metadata:any = null;
        let crateClass:string = "";

        if(crateMetadata[address]){
          logger.debug(`Crate Metadata By Address: Loaded key directly`);
          // metadata = { ...crateMetadata[address] };
          crateClass = crateMetadata[address];
        } else {
          logger.debug(`Crate Metadata By Address: Find key by string compare`);
          const metadataKeys = Object.keys(crateMetadata);
          for(let j = 0; j < metadataKeys.length; j++){
            const metadataKey = metadataKeys[j];
            if(metadataKey.toLowerCase() === address.toLowerCase()){
              //metadata = { ...crateMetadata[metadataKey] };
              //logger.info(`Crate Metadata By Address: found ${metadataKey}`);
              crateClass = crateMetadata[metadataKey];
              break;
            }
          }
        }

        //if(metadata){
        if(crateClass && CRATE_LIST[crateClass]){
          //remove privates attrs
          /* privateAttr.forEach(privy => {
            delete(metadata[privy]);
          }); */
          const Crate = CRATE_LIST[crateClass];
          //logger.info({ metadata }, `Crate Metadata By Address: Sending data`);
          res.send({
            name: Crate.getName(),
            description: Crate.getDescription(),
            image: Crate.getImage(),
            external_url: Crate.getExternalUrl(),
            animation_url: Crate.getAnimationUrl(),
            youtube_url: Crate.getYoutubeUrl(),
            attributes: Crate.getAttributes()
          });
        } else {
          logger.info(`Crate Metadata By Address: unknown address`);
          res.status(404).send({ error: "Crate information not found." });
        }

      } else {
        logger.info(`Crate Metadata By Address: metadata file not found`);
        res.status(404).send({ error: "Crate information not found." });
      }
    } else {
      return res.status(500).send({status: "Contract Address Not Specified"});
    }
  }
}

