import { EthereumAddress, getBunyanLogger, getEthereumConfig, getWeb3 } from "@playcheck/standard-lib";
import { ethers } from 'ethers';
import Web3 from "web3";
import BN from "bn.js";

export const TRANSACTION_CURRENTLY_PROCESSING = "TRANSACTION_CURRENTLY_PROCESSING";
export const TRANSACTION_ALREADY_PROCESSED = "TRANSACTION_ALREADY_PROCESSED";
export const DUPLICATE_TRANSACTION_HASH = "DUPLICATE_TRANSACTION_HASH";
export const INVALID_TRANSACTION_HASH = "INVALID_TRANSACTION_HASH";
export const TRANSACTION_HASH_REQUIRED = "Transaction Hash Is Required";

const logger = getBunyanLogger('br-api', { env: process.env.NODE_ENV });

export const makeClaimRewardStub = async (rewardId:string, recipient:EthereumAddress, amount: number) => {
  const configName = process.env.SIDE_CHAIN_ACCOUNT || 'sidechain-dev';
  const config = getEthereumConfig( configName );

  const web3 = getWeb3(config);

  // build claimReward function sig

  //logger.debug(`makeClaimRewardStub options`, config.walletOptions);

  let privateKeys: string[] = [];

  if(config.walletOptions.mnemonic){
    const wallet = ethers.Wallet.fromMnemonic(
      config.walletOptions.mnemonic?.phrase,
      config.walletOptions.derivationPath);

      privateKeys.push(wallet.privateKey);
      logger.debug(`signing wallet`, await wallet.getAddress());
  } else {
    privateKeys = config.walletOptions.privateKeys;
  }
  

  if(privateKeys.length === 0){
    logger.error(`Unable to retrieve private keys for system wallet from config ${configName}`);
    return false;
  }

  //logger.debug(`makeClaimRewardStub privateKeys`, privateKeys);

  const hash = web3.utils.soliditySha3(rewardId, recipient.$, amount);
  if(!hash){
    logger.error(`Unable to create command hash`, rewardId, recipient.$, amount);
    return false;
  }

  const { signature } = await web3.eth.accounts.sign(hash, privateKeys[0]);

  const r = signature.slice(2, 66);
  const s = signature.slice(66, 130);
  let v = signature.slice(130, 132);

  let type = "01";

  let _v = parseInt(v, 16);

  if( ![27, 28].includes(_v) ){
    v = (_v + 27).toString(16);
  } else {
    type = "00";
  }

  const recovered = await web3.eth.accounts.recover(hash, `0x${[r, s, v].join('')}`);

  logger.debug(`test recovery result: `, recovered);

  return `0x${[type, r, s, v].join('')}`;
}

export const signatureToBuffer = (sign:string) => {
  return Buffer.concat([
    // Buffer.from( Web3.utils.hexToBytes( `0x${sign.substring(2, 4)}` )), // type
    Buffer.from( Web3.utils.hexToBytes( `0x${sign.substring(4, 68)}` )), // r
    Buffer.from( Web3.utils.hexToBytes( `0x${sign.substring(68, 132)}` )), // s
    Buffer.from( Web3.utils.hexToBytes( `0x${sign.substring(132, 134)}` )), // v
  ]);
}

// dummy place holder for now
export const getEventData = async (eventId:string) => {
  return {
    id: eventId,
    name: `Event ${eventId}`,
    date: [Date.now(), Date.now()],
    price: 1,
  }
}