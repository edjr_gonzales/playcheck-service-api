const HDWalletProvider = require('@truffle/hdwallet-provider');
const Web3 = require('web3');
const sigUtil = require("eth-sig-util");
const chai = require('chai');
const chaiHttp = require('chai-http');

const os = require('os');
const fs = require('fs');
const path = require('path');

require('dotenv').config();

chai.use(chaiHttp);

const app = require('../../../dist/index.jsx');

const vars = require('../../test.vars');
const { hotPlugFile } = require('../../../dist/v1/auth/web3.jsx');
const { getEthereumConfig, getHDWalletProvider } = require('@playcheck/standard-lib');

const should = chai.should();
const expect = chai.expect;

const USER_MNEMONIC = vars.test_mnemonic;

const rootEthConfig = getEthereumConfig( process.env.MAIN_CHAIN_ACCOUNT );
const userEthConfig = JSON.parse(JSON.stringify(rootEthConfig));

userEthConfig.walletOptions.mnemonic.phrase = USER_MNEMONIC;

describe('POST /v1/auth/web3/login', () => {
  it('should return the user account of properly signed with signedType_vX', (done) => {
    const hdwallet = getHDWalletProvider(userEthConfig);

    const web3 = new Web3(hdwallet, {
      transactionPollingTimeout: 180,
      networkCheckTimeout: 180,
    });

    const wallet = hdwallet.getAddress(0);

    web3.eth.net.getNetworkType().then(network => {
      const msgParams = {
        types: {
          EIP712Domain:[
            {name:"name",type:"string"},
            {name:"network",type:"string"}
          ],
          Message: [{name:'message', type: 'string'}]
        },
        primaryType: 'Message',
        domain:{ name:"Battle Racers", network },

        message:{ message: "Login Test" }
      };
      
      web3.currentProvider.sendAsync({
        method: "eth_signTypedData_v3",
        params: [msgParams, wallet],
        from: wallet,
      }, (err, result) => {
        if(err){
          //console.log(err);
          done(err);
        } else {
          
          chai
          .request(app)
          .post('/v1/auth/web3/login')
          .set('content-type', 'application/x-www-form-urlencoded')
          .send({ address: wallet, message: JSON.stringify(msgParams), signed: result.result, type: 'typed' })
          .then((res) => {
            //console.log("Response", res.body, res.status);
            res.should.have.status(200);
            expect(res.body.address).to.be.equal(wallet);

            done();
          })
          .catch(err => {
            //console.log(err);
            done(err);
          });
        }
      });
    });
  }).timeout(0);

  it('should return the user account of properly signed with personal_sign', (done) => {
    const hdwallet = getHDWalletProvider(userEthConfig);

    const web3 = new Web3(hdwallet, {
      transactionPollingTimeout: 180,
      networkCheckTimeout: 180,
    });

    const wallet = hdwallet.getAddress(0);

    web3.eth.net.getNetworkType().then(network => {
      const msgParams = "Hello from Battleracers!";
      
      web3.currentProvider.sendAsync({
        method: "personal_sign",
        params: [web3.utils.asciiToHex(msgParams), wallet],
        from: wallet,
      }, (err, result) => {
        if(err){
          //console.log(err);
          done(err);
        } else {
          /* const recovered = sigUtil.recoverPersonalSignature({
            data: web3.utils.asciiToHex(msgParams),
            sig: result.result
          }); */

          chai
          .request(app)
          .post('/v1/auth/web3/login')
          .set('content-type', 'application/x-www-form-urlencoded')
          .send({ address: wallet, message: web3.utils.asciiToHex(msgParams), signed: result.result, type: 'personal' })
          .then((res) => {
            //console.log("Response", res.body.user, res.status);
            res.should.have.status(200);
            expect(res.body.address).to.be.equal(wallet);

            done();
          })
          .catch(err => {
            //console.log(err);
            done(err);
          });
        }
      });
    });
  }).timeout(0);
});

describe('Web3 Auth Maintenance Mode', function(){
  it('should return error code when maintenance mode is on', (done) => {
    fs.closeSync(fs.openSync(hotPlugFile, 'w'));

    chai
      .request(app)
      .get('/v1/auth/web3/getHash')
      .then((res) => {
        //console.log("Response", res.body, res.status);
        res.should.have.status(404);

        fs.unlinkSync(hotPlugFile);
        done();
      })
      .catch(err => {
        //console.log(err);
        fs.unlinkSync(hotPlugFile);
        done(err);
      }).finally(() => {
        
      });
  });
});