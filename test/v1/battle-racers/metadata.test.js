const HDWalletProvider = require('@truffle/hdwallet-provider');
const Web3 = require('web3');
const sigUtil = require("eth-sig-util");
const chai = require('chai');
const chaiHttp = require('chai-http');

const os = require('os');
const fs = require('fs');
const path = require('path');

chai.use(chaiHttp);

const app = require('../../../dist/index.jsx');

const vars = require('../../test.vars');
const { hotPlugFile } = require('../../../dist/v1/battle-racers/metadata.jsx');
const { exit } = require('process');
const { queryMappedClass, scanMappedClass } = require('@playcheck/standard-lib');
const { BRPartMetadata } = require('@playcheck/battle-racers');

const should = chai.should();
const expect = chai.expect;

describe('GET /v1/br/metadata/part/:id', function(){
  this.timeout(0);

  it('it should return the nft metadata information', async () => {
    // locate a metadata that actually exists
    let partMetadata = null;

    const i = await scanMappedClass({}, BRPartMetadata);
    
    for await (let part of i){
      console.log('found part', part);
      partMetadata = part;
      break;
    }

    expect(partMetadata).to.be.not.null;

    if(partMetadata){
      console.log(`subject found`, partMetadata);

      // send request
      const apiResult = await chai.request(app).get(`/v1/br/metadata/part/${partMetadata.id}`).send();

      // console.log(`api result`, apiResult.body);
      apiResult.should.have.status(200);
      expect(apiResult.body.external_url).to.equal(partMetadata.external_url);
    }
    
  }); 
});

describe('GET /v1/br/metadata/crate/:address', function(){
  this.timeout(0);

  it('it should return crate metadata information', async () => { 
    const contents = fs.readFileSync(path.resolve(process.cwd(), path.join('.', `crates-metadata.json`)), 'utf8');

    const crateMetadata = JSON.parse(contents);

    const addresses = Object.keys( crateMetadata );
    const selected = addresses[ Math.round(Math.random() * addresses.length - 1) ];

    console.log(`GET /v1/br/metadata/crate/${selected}`);
    const apiResult = await chai.request(app).get(`/v1/br/metadata/crate/${selected}`).send();

    console.log(`api result`, apiResult.body);
    apiResult.should.have.status(200);
  }); 
});

describe('Metadata Maintenance Mode', function(){
  it('should return error code when maintenance mode is on', (done) => {
    fs.closeSync(fs.openSync(hotPlugFile, 'w'));

    chai
      .request(app)
      .get('/v1/br/metadata/part/1')
      .then((res) => {
        // console.log("Response", res.body, res.status);
        res.should.have.status(404);

        fs.unlinkSync(hotPlugFile);
        done();
      })
      .catch(err => {
        // console.log(err);
        fs.unlinkSync(hotPlugFile);
        done(err);
      }).finally(() => {
        
      });
  });
});