const HDWalletProvider = require('@truffle/hdwallet-provider');
const Web3 = require('web3');
const sigUtil = require("eth-sig-util");
const chai = require('chai');
const chaiHttp = require('chai-http');

const os = require('os');
const fs = require('fs');
const path = require('path');

chai.use(chaiHttp);

const app = require('../../../dist/index.jsx');

const { getEthereumConfig, Wallet, createTypedSignature, getHDWalletProvider } = require('@playcheck/standard-lib');
const { CrateWood, CKCrate } = require('@playcheck/battle-racers');
const { SnowDeviceManagement } = require('aws-sdk');

const vars = require('../../test.vars');
const { DUPLICATE_TRANSACTION_HASH } = require('../../../dist/v1/battle-racers/index.jsx');
const { hotPlugFile } = require('../../../dist/v1/battle-racers/crate-opening.jsx');
const { exit } = require('process');

const should = chai.should();
const expect = chai.expect;

const USER_MNEMONIC = vars.test_mnemonic;

const rootEthConfig = getEthereumConfig( process.env.MAIN_CHAIN_ACCOUNT );
const userEthConfig = JSON.parse(JSON.stringify(rootEthConfig));

userEthConfig.walletOptions.mnemonic.phrase = USER_MNEMONIC;

describe('POST /v1/br/crate-draw/openCrate', function(){
  this.timeout(0);

  var auth_token = "";
  var wallet_id = "";

  before((done) => {
    // login to api server before anything else
    const hdwallet = getHDWalletProvider(userEthConfig);

    const web3 = new Web3(hdwallet, {
      transactionPollingTimeout: 180,
      networkCheckTimeout: 180,
    });

    const wallet = wallet_id = hdwallet.getAddress(0);

    //console.log(`crate opening: get login token for wallet`, wallet);

    const msgParams = {
      types: {
        EIP712Domain:[
          {name:"name",type:"string"},
          {name:"network",type:"string"}
        ],
        Message: [{name:'message', type: 'string'}]
      },
      primaryType: 'Message',
      domain:{ name:"Battle Racers", network: 1 },

      message:{ message: "Login Test" }
    };

    web3.eth.net.getNetworkType()
      .then(network => {
        msgParams.domain.network = network;
        return createTypedSignature(msgParams, hdwallet);
      })
      .then((sigResult) => {
        //console.log(`before all type sig result`, sigResult);

        if(sigResult){
          return chai
            .request(app)
            .post('/v1/auth/web3/login')
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({ address: wallet, message: JSON.stringify(msgParams), signed: sigResult, type: 'typed' });
        } else {
          done('Unable to sign data.');
        }
      })
      .then((loginResp) => {
        // //console.log(`Login Successful`, loginResp);
        auth_token = loginResp.body.tokenId;
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  before((done) => {
    const usrHdWallet = getHDWalletProvider(userEthConfig);
    const usrWallet = new Wallet(usrHdWallet.getAddress());

    // deposit 1 crate per test to user
    const woodCrate = new CrateWood(rootEthConfig);
    const ckCrate = new CKCrate(rootEthConfig);

    //console.log(`minting wood...`);
    woodCrate.mint(usrWallet, 1)
      .then(() => {
        return woodCrate.balanceOf(usrWallet);
      }).then((bal) => {
        //console.log(`wood balance after mint`, bal);

        if(bal > 0){
          //console.log(`minting ck...`);
          return ckCrate.mint(usrWallet, 1);
        } else {
          //console.log('Unable to mint wood crate.');
          done('Unable to mint wood crate.');
        }
      }).then(() => {
        return ckCrate.balanceOf(usrWallet);
      }).then((bal) => {
        //console.log(`ck balance after mint`, bal);
        if(bal > 0){
          done();
        } else {
          //console.log('Unable to mint CK crate.');
          done('Unable to mint CK crate.');
        }
      }).catch((err) => {
        //console.log('Error', err);
        done(err);
      });
  });

  it('it should accept unprocessed crate deposits', (done) => {
    // deposit a crate to system wallet
    const sysHdWallet = getHDWalletProvider(rootEthConfig);
    const sysWallet = new Wallet(sysHdWallet.getAddress());

    const woodCrate = new CrateWood(userEthConfig);

    let txHash = "";

    const onTxHash = (txResult) => {
      //console.log(`wood crate transfer result`, txResult);

      if(txHash !== ''){
        return chai.request(app)
        .post('/v1/br/crate-draw/openCrate')
        .set('content-type', 'application/x-www-form-urlencoded')
        .set('authorization', `${auth_token} ${wallet_id}`)
        .send({ tx: txHash, netId: userEthConfig.networkId, crateType: 'CrateWood' });
      }

      done('No Transaction Hash');
    };

    woodCrate.transfer(sysWallet, 1, {
      transactionHash: (hash) => {
        /** this callback is not the final value but instead is the receipt - proven in this test */
        //console.log(`wood crate transfer transaction hash`, hash);
        txHash = hash;
      },
      then: (txReceipt) => {
        if(txReceipt.status){
          txHash = txReceipt.transactionHash;
        }

        return txHash;
      }
    })
      .then(onTxHash)
      .then((postResult) => {
        // //console.log(`open crate post result`, postResult);
        expect(postResult.body.success).to.be.true;

        // check that server will not accept existing crate tx hash
        // this time we should receive an error
        return chai.request(app)
          .post('/v1/br/crate-draw/openCrate')
          .set('content-type', 'application/x-www-form-urlencoded')
          .set('authorization', `${auth_token} ${wallet_id}`)
          .send({ tx: txHash, netId: userEthConfig.networkId, crateType: 'CrateWood' });

        // done();
      })
      .then((errorResult) => {
        errorResult.should.have.status(400);
        expect(!!errorResult.body.success).to.be.false;
        expect(errorResult.body.error).to.equal(DUPLICATE_TRANSACTION_HASH);
        done();
      })
      .catch((err) => {
        //console.log(`wood crate transfer catch`, err);
        onTxHash(txHash);
      });
  }); 
});

describe('Crate Opening Maintenance Mode', function(){
  it('should return error code when maintenance mode is on', (done) => {
    fs.closeSync(fs.openSync(hotPlugFile, 'w'));

    chai
      .request(app)
      .get('/v1/br/crate-draw/crateStatus')
      .then((res) => {
        //console.log("Response", res.body, res.status);
        res.should.have.status(404);

        fs.unlinkSync(hotPlugFile);
        done();
      })
      .catch(err => {
        //console.log(err);
        fs.unlinkSync(hotPlugFile);
        done(err);
      }).finally(() => {
        
      });
  });
});