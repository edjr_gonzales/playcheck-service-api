const chai = require('chai');
const chaiHttp = require('chai-http');

const { getEthereumConfig, getHDWalletProvider, Wallet } = require('@playcheck/standard-lib');

const os = require('os');
const fs = require('fs');
const path = require('path');

chai.use(chaiHttp);

const app = require('../../../dist/index.jsx');

const vars = require('../../test.vars');
const utils = require('../../test.utils');
const { hotPlugFile } = require('../../../dist/v1/battle-racers/scrap-services.jsx');
const { signatureToBuffer } = require('../../../dist/v1/battle-racers/index.jsx');
const { ACCOUNT_NOT_PERMITTED } = require('../../../dist/v1/auth/index.jsx');
const { Scrap } = require('@playcheck/battle-racers');

const should = chai.should();
const expect = chai.expect;

const rootEthConfig = getEthereumConfig( process.env.MAIN_CHAIN_ACCOUNT );
const rootSideConfig = getEthereumConfig( process.env.SIDE_CHAIN_ACCOUNT );

const userEthConfig = JSON.parse(JSON.stringify(rootEthConfig));
const userSideConfig = JSON.parse(JSON.stringify(rootSideConfig));

userEthConfig.walletOptions.mnemonic.phrase = vars.test_mnemonic;
userSideConfig.walletOptions.mnemonic.phrase = vars.test_mnemonic;

const rootWallet = getHDWalletProvider(rootEthConfig);
const userWallet = getHDWalletProvider(userEthConfig);

let rootSession = "";
let userSession = "";

describe('POST /v1/br/scrap/createReward', function(){
  this.timeout(0);

  before((done) => {
    utils.createUserSessionToken(chai.request(app), rootEthConfig)
      .then((sessionToken) => {
        if(sessionToken){
          rootSession = sessionToken;
          console.log("rootSession", rootSession);
          return utils.createUserSessionToken(chai.request(app), userEthConfig);
        } else {
          done('System account cant login.');
        }
      }).then((sessionToken) => {
        if(sessionToken){
          userSession = sessionToken;
          console.log("userSession", userSession);
          done();
        } else {
          done('User account cant login.');
        }
        
      }).catch((error) => {
        done(error);
      });
  });

  it('allows system user to grant rewards', async () => {
    /* console.log("rootSession", rootSession);
    console.log("userSession", userSession); */

    // create rewards 3 times for further testing
    let i = 0;
    while(i < 3){
      i++;

      const requestResp = await chai.request(app)
        .post('/v1/br/scrap/createReward')
        .set('content-type', 'application/x-www-form-urlencoded')
        .set('authorization', rootSession)
        .send({ recipient: userWallet.getAddress(), amount: 5 });

      requestResp.should.have.status(200);

      expect(requestResp.body.success).to.be.true;
      expect(requestResp.body).to.have.property('reward');
    }
  });

  it('disallows non-system user to grant rewards', async () => {
    const requestResp = await chai.request(app)
      .post('/v1/br/scrap/createReward')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('authorization', userSession)
      .send({ recipient: userWallet.getAddress(), amount: 5 });

    requestResp.should.have.status(403);

    expect(requestResp.body).to.have.property('error');
    expect(requestResp.body.error).to.equal(ACCOUNT_NOT_PERMITTED);
  });
});

describe('GET /v1/br/scrap/listPendingRewards', function(){
  this.timeout(0);

  before((done) => {
    if(rootSession === ''){
      utils.createUserSessionToken(chai.request(app), rootEthConfig)
        .then((sessionToken) => {
          if(sessionToken){
            rootSession = sessionToken;
            console.log("rootSession", rootSession);
            return utils.createUserSessionToken(chai.request(app), userEthConfig);
          } else {
            done('System account cant login.');
          }
        }).then((sessionToken) => {
          if(sessionToken){
            userSession = sessionToken;
            console.log("userSession", userSession);
            done();
          } else {
            done('User account cant login.');
          }
          
        }).catch((error) => {
          done(error);
        });
    } else {
      done();
    }
  });

  it('allows user to list the rewards', async () => {
    const requestResp = await chai.request(app)
      .get('/v1/br/scrap/listPendingRewards')
      .set('authorization', userSession)
      .send();

    requestResp.should.have.status(200);

    expect(requestResp.body.success).to.be.true;
    expect(requestResp.body).to.have.property('rewards');
    expect(requestResp.body.rewards).to.be.an('array');
  });
});

describe('POST /v1/br/scrap/claimReward', function(){
  this.timeout(0);

  before((done) => {
    if(rootSession === ''){
      utils.createUserSessionToken(chai.request(app), rootEthConfig)
        .then((sessionToken) => {
          if(sessionToken){
            rootSession = sessionToken;
            console.log("rootSession", rootSession);
            return utils.createUserSessionToken(chai.request(app), userEthConfig);
          } else {
            done('System account cant login.');
          }
        }).then((sessionToken) => {
          if(sessionToken){
            userSession = sessionToken;
            console.log("userSession", userSession);
            done();
          } else {
            done('User account cant login.');
          }
          
        }).catch((error) => {
          done(error);
        });
    } else {
      done();
    }
  });

  it('allows user to claim single reward', async () => {
    const pendingReq = await chai.request(app)
      .get('/v1/br/scrap/listPendingRewards')
      .set('authorization', userSession)
      .send();

    const [reward] = pendingReq.body.rewards;

    const requestResp = await chai.request(app)
      .post('/v1/br/scrap/claimReward')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('authorization', userSession)
      .send({ recipient: userWallet.getAddress(), rewards: [ reward.rewardId ] });

    requestResp.should.have.status(200);

    expect(requestResp.body).to.have.property('signature');
    expect(requestResp.body).to.have.property('rewardId');
    expect(requestResp.body).to.have.property('rewards');
    expect(requestResp.body).to.have.property('amount');
    expect(requestResp.body.rewards).to.be.an('array').that.is.not.empty;
    expect(requestResp.body.rewards).to.eql([ reward.rewardId ]);
    expect(requestResp.body.amount).to.equal( Number(reward.amount) );

    console.log('claim sig', requestResp.body.signature);

    // perform claim on contract
    const usrHdWallet = getHDWalletProvider(userSideConfig);
    const usrWallet = new Wallet(usrHdWallet.getAddress());

    const scrap = new Scrap(userSideConfig);
    let claimTxHash = "";

    console.log(`claiming reward`, requestResp.body.rewardId, requestResp.body.amount, requestResp.body.signature);
    console.log(`claiming reward`, signatureToBuffer(requestResp.body.signature));

    await scrap.claimReward(
      requestResp.body.rewardId, requestResp.body.amount, `0x${requestResp.body.signature.slice(4)}`,
      {
        'transactionHash': (txHash) => {
          claimTxHash = txHash;
        }
      }).catch((error) => {
        console.log(`claim error`, error);
      });

    console.log('claim txHash', claimTxHash);

    const claimResp = await chai.request(app)
      .post(`/v1/br/scrap/setClaimTxn/${requestResp.body.rewardId}`)
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('authorization', userSession)
      .send({ txHash: claimTxHash });

    console.log('set claim tx hash response', claimResp.body);
  });

  /* it('allows user to claim multiple reward', async () => {
    const pendingReq = await chai.request(app)
      .get('/v1/br/scrap/listPendingRewards')
      .set('authorization', userSession)
      .send();

    const rewardsIds = pendingReq.body.rewards.map(reward => reward.rewardId);

    let rewardsAmt = 0;
    pendingReq.body.rewards.forEach(reward => rewardsAmt += Number(reward.amount) );

    const requestResp = await chai.request(app)
      .post('/v1/br/scrap/claimReward')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('authorization', userSession)
      .send({ recipient: userWallet.getAddress(), rewards: rewardsIds });

    requestResp.should.have.status(200);

    expect(requestResp.body).to.have.property('signature');
    expect(requestResp.body).to.have.property('rewardId');
    expect(requestResp.body).to.have.property('rewards');
    expect(requestResp.body).to.have.property('amount');
    expect(requestResp.body.rewards).to.be.an('array').that.is.not.empty;
    expect(requestResp.body.rewards).to.eql( rewardsIds );
    expect(requestResp.body.amount).to.equal( rewardsAmt );
  }); */
});

/* describe('Scrap API Maintenance Mode', function(){
  it('should return error code when maintenance mode is on', (done) => {
    fs.closeSync(fs.openSync(hotPlugFile, 'w'));

    chai
      .request(app)
      .post('/v1/br/scrap/createReward')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('authorization', rootSession)
      .send({ recipient: userWallet.getAddress(), amount: 5 })
      .then((res) => {
        console.log("Response", res.body, res.status);
        res.should.have.status(404);

        fs.unlinkSync(hotPlugFile);
        done();
      })
      .catch(err => {
        // console.log(err);
        fs.unlinkSync(hotPlugFile);
        done(err);
      }).finally(() => {
        
      });
  });
}); */