const Web3 = require('web3');
const { createTypedSignature, getHDWalletProvider } = require('@playcheck/standard-lib');

const createUserSessionToken = async (chaiRequest, userEthConfig) => {
  const hdwallet = getHDWalletProvider(userEthConfig);

  const web3 = new Web3(hdwallet, {
    transactionPollingTimeout: 180,
    networkCheckTimeout: 180,
  });

  const wallet = wallet_id = hdwallet.getAddress(0);

  const network = await web3.eth.net.getNetworkType();

  const msgParams = {
    types: {
      EIP712Domain:[
        {name:"name",type:"string"},
        {name:"network",type:"string"}
      ],
      Message: [{name:'message', type: 'string'}]
    },
    primaryType: 'Message',
    domain:{ name:"Battle Racers", network },

    message:{ message: "Login Test" }
  };

  const sigResult = await createTypedSignature(msgParams, hdwallet);

  if(sigResult){
    const loginResp = await chaiRequest.post('/v1/auth/web3/login')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send({ address: wallet, message: JSON.stringify(msgParams), signed: sigResult, type: 'typed' });

    return `${loginResp.body.tokenId} ${wallet}`
  } else {
    return false;
  }
}

module.exports = {
  createUserSessionToken
}