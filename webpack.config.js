const path = require('path');
const nodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: ["./src/index.tsx"],

  devtool: 'source-map',

  target: 'node',

  externals: [nodeExternals()],

  optimization: {
    nodeEnv: false
  },
  
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'server.js',
    libraryTarget: 'this'
  },

  module: {
    rules: [
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      },
      {
        test: /\.scss$/,
        //use: MiniCssExtractPlugin.loader({ fallback: 'style-loader', use: [ 'css-loader', 'sass-loader' ] })
        loader: 'scss-extract-loader'
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
      /* {
        test: /\.js$|\.es6|\.jsx$/,
        loader: 'babel-loader'
      }, */
      { 
        test: /\.tsx?$|\.es6|\.jsx?$/, 
        loader: "ts-loader",
        options: {
          logLevel: 'error'
        }
      }
    ]
  },
  resolve: {
    extensions: ['.es6', '.js', '.jsx', '.ts', '.tsx'],
    modules: ['node_modules']
  },
  plugins: [
    new MiniCssExtractPlugin({ filename: "styles.css" }),
  ]
};
